# Overview #

REST API for SoaPoints utilizing Node, Express, and MongoDB.

Example UIs for this API can be found here:
* https://bitbucket.org/soatech/soapoints-react-ui
* https://bitbucket.org/soatech/soapoints-react-native

# Installation #

## Linux ##

```
#!bash

$ apt-get install mongodb
$ apt-get install libkrb5-dev
```

## OSX ##

```
#!bash

$ brew install mongodb
```

## Windows ##

LOL Nope

# Running Server #

```
#!bash

$ cd soapoints-mean-api
$ npm install
$ node server.js
```

# REST Calls #

Protected calls need the following headers:

Authorization: JWT ${token}

### /auth/login ###

**POST**

Finds the user in the system and generates a token.  If a user isn't found returns status 404 with a 

* Method: POST
* Authentication Required: False

Example Body:

```
#!JSON

{
  "username": "jsmeggar",
  "password": "hunter2"
}
```

### /auth/register ###

**POST**

Creates a new user and generates a JWT token for them.  Returns the newly created user object with token.

* Method: POST
* Authentication Required: False

Example Body:

```
#!JSON

{
  "firstName": "Joe",
  "lastName": "Smeggar",
  "email": "jsmeggar@example.com",
  "username": "jsmeggar",
  "password": "hunter2"
}
```

### /auth/logout ###

**GET**

Nullifies the current user's token.

* Method: GET
* Authentication Required: True

### /users ###

**GET**

Lists all users in the system.

* Method: GET
* Authentication Required: True

**POST**

Creates a new User.  Unlike Register, this does not generate an authentication token.

* Method: POST
* Authentication Required: True

Example Body:

```
#!JSON

{
  "firstName": "Joe",
  "lastName": "Smeggar",
  "email": "jsmeggar@example.com",
  "username": "jsmeggar",
  "password": "hunter2"
}
```

### /users/:userId ###

**GET**

Returns a single User object for that ID.

* Method: GET
* Authentication Required: True

**PUT**
Updates the user with new values.

* Method: PUT
* Authentication Required: True

Example Body:

```
#!JSON

{
  "firstName": "Joe",
  "lastName": "Smeggar",
  "email": "jsmeggar@example.com"
}
```

**DELETE**

Deletes the user from the system.

* Method: DELETE
* Authentication Required: True

### /tasks ###

**GET**

Returns a list of all tasks in the system.  Not filtered based on user or frequency.

* Method: GET
* Authentication Required: True

**POST**

Creates a new task.

* Method: POST
* Authentication Required: True

Example for creating a shared task:

```
#!JSON

{
  "name": "Dishes",
  "description": "Even the big ones!",
  "repeatable": true,
  "frequencyType": "Daily",
  "weekDay": "",
  "soaPoints": 1.1,
  "startDate": null,
  "endDate": null
}
```

To create a task specific to a user, you would just add the user id as a reference.

### /tasks/:taskId ###

**GET**

Returns the Task object for that ID.

* Method: GET
* Authentication Required: True

**PUT**

Updates the task.

* Method: PUT
* Authentication Required: True

Example Body: See Create.

**DELETE**

Deletes the task permanently.

* Method: DELETE
* Authentication Required: True

### /sharedTasks ###

**GET**

Filter tasks down to ones that don't belong to a user.

* Method: GET
* Authentication Required: True

### /sharedTasks/:frequencyType ###

**GET**

Filter even further down to frequencyType (Daily, Weekly, Monthly, Annually).

* Method: GET
* Authentication Required: True

### /mytasks/:userId ###

**GET**

Filter tasks to just the requested user.

* Method: GET
* Authentication Required: True

### /mytasks/:userId/:frequencyType ###

**GET**

Filter user tasks by frequencyType.

* Method: GET
* Authentication Required: True