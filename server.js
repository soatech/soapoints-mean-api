process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var mongoose = require('./config/mongoose');
var express = require('./config/express');
var passport = require('./config/passport');

var db = mongoose();
var app = express();
// TODO: I don't think this is a very wise naming approach
var passport = passport();

app.listen(3060);
module.exports = app;

console.log("Server running at http://localhost:3060/");

