var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');

var UserSchema = new Schema({
    firstName: String,
    lastName: String,
    email: {
        type: String,
        // email validation is fun
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    username: {
        type: String,
        trim: true, // prevent leading/following whitespace
        unique: true,
        required: true
    },
    password: {
        type: String,
        trim: true, // prevent leading/following whitespace
        required: true,
        validate: [
            function(password) {
                // TODO: Other password restrictions
                return password.length >= 6;
            },
            'Password should be longer'
        ],
        get: function(password) {
            // We don't want to ever return the password
            // TODO: this breaks things badly.  Figure out another way to hide this from JSON output while still getting access internally
            return password;
        }
    },
    salt: String,
    provider: {
        type: String,
        required: true
    },
    providerId: String,
    providerData: {},
    role: {
        type: String,
        // TODO: move these to an enum/typecode class
        enum: ['Admin', 'User']
    },
    token: String,
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

UserSchema.virtual('fullName').get(function() {
    return this.firstName + ' ' + this.lastName;
});

// Add a helper for logging in
UserSchema.statics.findOneByUsername = function (username, callback) {
    this.findOne({ username: new RegExp(username, 'i') }, callback);
};

UserSchema.pre('save', function(next) {
    if(this.password) {
        this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
        this.password = this.hashPassword(this.password);
    }

    this.updated = new Date();

    next();
});

UserSchema.methods.hashPassword = function(password) {
    return crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('base64');
};

UserSchema.methods.authenticate = function(password) {
    return this.password === this.hashPassword(password);
};

UserSchema.set('toJSON', {virtuals: true, getters: true, setters: true});

// TODO: better example of setters and getters
// TODO: example of instance methods
// TODO: Error handling/response encapsulation
// TODO: Examples of pre and post middleware
// TODO: DBRef vs. id field for Tasks (pg. 122)

mongoose.model('User', UserSchema);