var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TaskSchema = new Schema({
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    name: {
        type: String,
        required: true
    },
    description: String,
    repeatable: {
        type: Boolean,
        default: false
    },
    frequencyType: {
        type: String,
        // TODO: move these to enum/typecode class
        enum: ['Daily', 'Weekly', 'Monthly', 'Annually']
    },
    frequencyQuantifier: Number,
    weekDay: String,
    soaPoints: Number,
    startDate: Date,
    endDate: Date,
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

TaskSchema.pre('save', function(next) {
    this.updated = new Date();

    next();
});

mongoose.model('Task', TaskSchema);