var Task = require('mongoose').model('Task');

exports.listSharedTasks = function(req, res, next) {
    var conditions = {
        user: null
    };

    if(req.frequencyType) {
        conditions.frequencyType = req.frequencyType;
    }

    Task.find(conditions, function(err, tasks) {
        if(err) {
            console.log(err);
            next(err);
        } else {
            res.json(tasks);
        }
    });
};

exports.listUserTasks = function(req, res, next) {
    var conditions = {};

    // TODO: fail if we don't have a user
    if(req.user) {
        conditions.user = req.user;
    }

    if(req.frequencyType) {
        conditions.frequencyType = req.frequencyType;
    }

    Task.find(conditions, function(err, tasks) {
        if(err) {
            next(err);
        } else {
            res.json(tasks);
        }
    });
};

exports.read = function(req, res, next) {
    res.json(req.task);
};

exports.create = function(req, res, next) {
    var task = new Task(req.body);

    task.save(function(err) {
        if(err) {
            return next(err);
        } else {
            res.json(task);
        }
    });
};

exports.update = function(req, res, next) {
    Task.findByIdAndUpdate(req.task.id, req.body, {new: true}, function(err, task) {
        if(err) {
            return next(err);
        } else {
            res.json(task);
        }
    });
};

exports.remove = function(req, res, next) {
    req.task.remove(function(err) {
        if(err) {
            return next(err);
        } else {
            // TODO: not sure if we should return the removed task or not
            res.json(req.task);
        }
    });
};

exports.taskById = function(req, res, next, id) {
    Task.findOne({
        _id: id
    }, function(err, task) {
        if(err) {
            return next(err);
        } else {
            req.task = task;
            next();
        }
    });
};

exports.tasksByFrequency = function(req, res, next, frequencyType) {
    req.frequencyType = frequencyType;

    next();
};