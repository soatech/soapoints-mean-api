var User = require('mongoose').model('User');
var passport = require('passport');

exports.list = function(req, res, next) {
    User.find({}, function(err, users) {
        if(err) {
            return next(err);
        } else {
            res.json(users);
        }
    });
};

// userById gets called first to get the user from the db and attach it to the req
exports.read = function(req, res, next) {
    res.json(req.user);
};

exports.create = function(req, res, next) {
    var user = new User(req.body);

    user.save(function(err) {
        if(err) {
            return next(err);
        } else {
            res.json(user);
        }
    });
};

// userById gets called first to get the user from the db and attach it to the req
exports.update = function(req, res, next) {
    // new flag is required if you want it to return the updated document
    User.findByIdAndUpdate(req.user.id, req.body, {new: true}, function(err, user) {
        if(err) {
            return next(err);
        } else {
            res.json(user);
        }
    });
};

// userById gets called first to get the user from the db and attach it to the req
exports.remove = function(req, res, next) {
    req.user.remove(function(err) {
        if(err) {
            return next(err);
        } else {
            res.json(req.user);
        }
    });
};

exports.userById = function(req, res, next, id) {
    User.findOne({
        _id: id
    }, function(err, user) {
        if(err) {
            return next(err);
        } else {
            req.user = user;
            next();
        }
    });
};