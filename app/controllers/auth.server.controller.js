var User = require('mongoose').model('User');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var config = require('../../config/config.js');

// TODO: do something with messages in general
var getErrorMessage = function(err) {
    var message = '';

    if(err.code) {
        switch(err.code) {
            case 11000:
            case 11001:
                message = "Username already exists";
                break;
            default:
                message = "Something went wrong";
        }
    } else {
        for (var errName in err.errors) {
            if(err.errors[errName].message) message = err.errors[errName].message;
        }
    }

    return message;
};

generateToken = function(user) {
    return jwt.sign(user, config.sessionSecret, {
        expiresIn: 1440*60 // expires in 24 hours
    });
};

exports.isAuthenticated = passport.authenticate('jwt', { session : false });

//exports.isAuthenticated = function(a,b,c,d) {
//    return true;
//};

exports.login = function(req, res, next) {
    User.findOneByUsername(req.body.username, function(err, user) {
        if(err) {
            return next(err);
        } else if (user === null || !user.authenticate(req.body.password)) {
            res.status('404').json({error: 'User Not Found'});
        } else {
            // if user is found and password is right
            // create a token
            // TODO: if you bring the token back from the DB then the generated token just gets bigger and bigger
            user.token = null;

            var token = generateToken(user);

            User.findByIdAndUpdate(user.id, {token: token, updated: new Date()}, {new: true}, function(err, user) {
                if(err) {
                    return next(err);
                } else {
                    res.json(user);
                }
            });
        }
    });
};

exports.register = function(req, res, next) {
    if(!req.user) {
        var user = new User(req.body);

        // log them in while we are at it.
        user.token = generateToken(user);

        var message = null;

        user.provider = 'jwt';

        user.save(function(err) {
            if(err) {
                message = getErrorMessage(err);

                res.end(message);
            }

            res.json(user);
        });
    } else {
        res.status('404');
        res.end("User not found");
    }
};

exports.logout = function(req, res, next) {
    // TODO: for some reason doing these chains makes the call break and do a 404.  Maybe optimize this a bit
    // get the token off the headers and find the user
    var authHeader = req.headers.authorization;

    // strip the JWT bit
    var token = authHeader.substr(4, authHeader.length-4);

    // TODO: The reason we have to keep doing it this way is because of the password save stuff.
    // It will re-hash the password and save that to the db.
    // So we need a better way to handle passwords
    User.findOneAndUpdate({token: token}, {token: null, updated: new Date()}, {new: true}, function(err, user) {
        if(err) {
            next(err);
        } else {
            if(user) {
                res.end("token cleared");
            }
        }
    });
};