var authController = require('../../app/controllers/auth.server.controller');

module.exports = function(app) {
    app.route('/auth/register')
        .post(authController.register);

    app.route('/auth/login')
        .post(authController.login);

    app.route('/auth/logout')
        .get(authController.isAuthenticated, authController.logout);
};