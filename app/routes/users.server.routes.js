var users = require('../../app/controllers/users.server.controller');
var passport = require('passport');
var authController = require('../../app/controllers/auth.server.controller');

// TODO: Find a better way to protect routes instead of one at a time
module.exports = function(app) {
    app.route('/users')
        .get(authController.isAuthenticated, users.list)
        .post(authController.isAuthenticated, users.create);

    app.route('/users/:userId')
        .get(authController.isAuthenticated, users.read)
        .put(authController.isAuthenticated, users.update)
        .delete(authController.isAuthenticated, users.remove);

    // this applies to every route that has the :userId and will trigger first
    app.param('userId', users.userById);
};