var tasksController = require('../../app/controllers/tasks.server.controller');
var userController = require('../../app/controllers/users.server.controller');
var authController = require('../../app/controllers/auth.server.controller');

module.exports = function(app) {
    app.route('/tasks')
        .get(authController.isAuthenticated, tasksController.listSharedTasks)
        .post(authController.isAuthenticated, tasksController.create);

    app.route('/tasks/:taskId')
        .get(authController.isAuthenticated, tasksController.read)
        .put(authController.isAuthenticated, tasksController.update)
        .delete(authController.isAuthenticated, tasksController.remove);

    app.route('/sharedtasks')
        .get(authController.isAuthenticated, tasksController.listSharedTasks);

    app.route('/sharedtasks/:frequencyType')
        .get(authController.isAuthenticated, tasksController.listSharedTasks);

    app.route('/mytasks/:userId')
        .get(authController.isAuthenticated, tasksController.listUserTasks);

    app.route('/mytasks/:userId/:frequencyType')
        .get(authController.isAuthenticated, tasksController.listUserTasks);

    app.param('userId', userController.userById);
    app.param('taskId', tasksController.taskById);
    app.param('frequencyType', tasksController.tasksByFrequency);
};