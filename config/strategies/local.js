var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('mongoose').model('User');

module.exports = function() {
    passport.use(new LocalStrategy(function(username, password, done) {
        User.findOneByUsernameAndPassword(username, password, function(err, user) {
            if(err) {
                return done(err);
            }

            if(user === null || !user.authenticate(password)) {
                return done(null, false, {
                    message: 'Invalid Username or Password'
                });
            }

            return done(null, user);
        });
    }));
};