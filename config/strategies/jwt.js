var passport = require('passport');
var User = require('mongoose').model('User');
var JwtStrategy = require('passport-jwt').Strategy;

module.exports = function() {
    var opts = {};
    opts.secretOrKey = '!6FTGUZhs7Y^yn8JCN5XYnFgnPypTru8';
    opts.passReqToCallback = true;
    passport.use(new JwtStrategy(opts, function(req, jwt_payload, done) {
        // TODO: For some reason it caches this payload like a mother fucker yet it has no token object on it
        // deleting the user from the db doesn't stop it.
        // I have no idea where it gets this payload
        // and i have no idea why we have to do another look-up when it clearly has a legit user already
        // so for now, just return the payload

        // The idea for this second step of validation is because JWT caches these tokens until they expire
        // So we need to take the user it cached and make sure they should still be logged in
        // for some reason we don't have access to the token at this point (aside from reparsing the request)
        // But what we can do is just check the database for this user and see if they have a token assigned
        // Then on logout we should update the user and nullify their token
        User.findOne({_id: jwt_payload.id}, function(err, user) {
            if (err) {
                return done(err, false);
            }
            if (user && user.token) {
                done(null, user);
            } else {
                done(null, false);
            }
        });
    }));
};
