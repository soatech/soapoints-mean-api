var passport = require('passport');
var BearerStrategy = require('passport-http-bearer').Strategy;
var User = require('mongoose').model('User');

module.exports = function() {
    passport.use(new BearerStrategy(function(token, done) {
        User.findOne({
            token: token
        }, function(err, user) {
            if(err) {
                return done(err);
            }

            if(user === null) {
                return done(null, false);
            }

            return done(null, user, { scope: 'all'});
        });
    }));
};