var passport = require('passport');
var DigestStrategy = require('passport-http').DigestStrategy;
var User = require('mongoose').model('User');

module.exports = function() {
    passport.use(new DigestStrategy( { qop: 'auth'}, function(username, done) {
        User.findOneByUsername(username, function(err, user) {
            if(err) {
                return done(err);
            }

            if(user === null) {
                return done(null, false);
            }

            return done(null, user, user.password);
        });
    }, function(params, done) {
        // validate nonce?

        done(null, true);
    }));
};